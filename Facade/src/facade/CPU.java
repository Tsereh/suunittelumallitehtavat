/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author kiril
 */
public class CPU {
    private long position = 0;
    
    
    public void freeze() {
        System.out.println("CPU freezed");
    }
    public void jump(long position) {
        this.position = position;
        System.out.println("CPU jumped to " + position);
    }
    public void execute() {
        System.out.println("Executing memory location: " + position);
    }
}
