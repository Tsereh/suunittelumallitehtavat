/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;

/**
 *
 * @author kiril
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pikaruokala pikaruokala = new Pikaruokala();
        KerroshampurilainenBuilder hesebuilder = new HesburgerKHBuilder();
        KerroshampurilainenBuilder mäkkibuilder = new McDonaldsKHBuilder();
        
        System.out.println("Hese");
        pikaruokala.setKHBuilder(hesebuilder);
        pikaruokala.constructKerroshampurilainen();
        ArrayList hesehampurilainen = (ArrayList) pikaruokala.getKerroshampurilainen();
        for(Object h : hesehampurilainen) {
            System.out.println(h);
        }
        
        System.out.println("Mäkki");
        pikaruokala.setKHBuilder(mäkkibuilder);
        pikaruokala.constructKerroshampurilainen();
        StringBuilder mäkkihampurilainen = (StringBuilder) pikaruokala.getKerroshampurilainen();
        for(int i=0; i<mäkkihampurilainen.length(); i++) {
            System.out.println(mäkkihampurilainen.charAt(i));
        }
    }
    
}
