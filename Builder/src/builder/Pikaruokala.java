/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author kiril
 */

// Director
public class Pikaruokala {
    private KerroshampurilainenBuilder KHBuilder;
    
    public void setKHBuilder(KerroshampurilainenBuilder khb) {
        KHBuilder = khb;
    }
    
    public Object getKerroshampurilainen() {
        return KHBuilder.getKerroshampurilainen();
    }
    
    public void constructKerroshampurilainen() {
        KHBuilder.buildSampyla();
        KHBuilder.buildPihvi();
        KHBuilder.buildMauste();
    }
}
