/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author kiril
 */

// abstract builder
abstract class KerroshampurilainenBuilder {
    
    public abstract Object getKerroshampurilainen();
    
    public abstract void buildSampyla();
    public abstract void buildPihvi();
    public abstract void buildMauste();
    
}
