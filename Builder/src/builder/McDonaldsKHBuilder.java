/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author kiril
 */

// concrete builder
public class McDonaldsKHBuilder extends KerroshampurilainenBuilder {
    StringBuilder hampurilainen = new StringBuilder();

    @Override
    public Object getKerroshampurilainen() {
        return hampurilainen;
    }

    @Override
    public void buildSampyla() {
        hampurilainen.append("sampyla");
    }

    @Override
    public void buildPihvi() {
        hampurilainen.append("pihvi");
    }

    @Override
    public void buildMauste() {
        hampurilainen.append("mauste");
    }
    
}
