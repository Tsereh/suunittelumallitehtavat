/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kiril
 */

// concrete builder
public class HesburgerKHBuilder extends KerroshampurilainenBuilder {
    List hampurilainen = new ArrayList<Object>();

    @Override
    public void buildSampyla() {
        hampurilainen.add(new Sampyla());
    }

    @Override
    public void buildPihvi() {
        hampurilainen.add(new Pihvi());
    }

    @Override
    public void buildMauste() {
        hampurilainen.add(new Mauste());
    }

    @Override
    public Object getKerroshampurilainen() {
        return hampurilainen;
    }
    
}
