/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author kiril
 */
public class ChainOfResponsibility {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EsimiesHandler eh = new EsimiesHandler(new PaallikkoHandler(new ToimitusjohtajaHandler(null)));
        
        eh.handle(2000, 2020);
        eh.handle(2000, 2050);
        eh.handle(2000, 2200);
    }
    
}
