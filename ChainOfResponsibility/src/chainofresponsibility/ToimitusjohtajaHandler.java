/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author kiril
 */
public class ToimitusjohtajaHandler extends Handler {
    
    public ToimitusjohtajaHandler(Handler s) {
        super(s, 99999999);
    }
    
    public boolean handle(double alkuPalkka, double loppuPalkka) {
        boolean onnistui = super.handle(alkuPalkka, loppuPalkka);
        if(onnistui) {
            System.out.println("Toimitusjohtaja ei hyväksy palkankorotusta.");
        }
        return onnistui;
    }
    
}
