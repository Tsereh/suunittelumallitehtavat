/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author kiril
 */
public abstract class Handler {
    private Handler successor;
    public double maxKorotus;
    
    public Handler(Handler s, double maxKorotus) {
        successor = s;
        this.maxKorotus = maxKorotus;
    }
    
    public boolean handle(double alkuPalkka, double loppuPalkka) {
        boolean onnistuu = false;
        if(loppuPalkka/alkuPalkka <= maxKorotus) {
            onnistuu = true;
        } else if (successor != null) {
            successor.handle(alkuPalkka, loppuPalkka);
        }
        return onnistuu;
    }
}
