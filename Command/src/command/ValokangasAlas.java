/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author kiril
 */
public class ValokangasAlas implements Command {
    
    private Valokangas valokangas;

    public ValokangasAlas(Valokangas valokangas) {
        this.valokangas = valokangas;
    }

    @Override
    public void execute() {
        valokangas.laske();
    }
    
}
