/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author kiril
 */
public class Nappi {
    
    Command cmd;
    
    public Nappi(Command cmd) {
        this.cmd = cmd;
    }
    
    public void paina() {
        cmd.execute();
    }
    
}
