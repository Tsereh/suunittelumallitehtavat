/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author kiril
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Valokangas valokangas = new Valokangas();
        
        Command nosta = new ValokangasYlos(valokangas);
        Command laske = new ValokangasAlas(valokangas);
        
        Nappi ylos_nappi = new Nappi(nosta);
        Nappi alas_nappi = new Nappi(laske);
        
        ylos_nappi.paina();
        alas_nappi.paina();
    }
    
}
