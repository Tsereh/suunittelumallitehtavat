/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author Kiril Tsereh
 */
public class AdidasOutfitFactory implements OutfitFactory {

    
    public Paahine makePaahine() {
        return new AdidasLippis();
    }

    @Override
    public Housut makeHousut() {
        return new AdidasFarkut();
    }

    @Override
    public Paita makePaita() {
        return new AdidasTPaita();
    }

    @Override
    public Jalkineet makeJalkineet() {
        return new AdidasKengat();
    }
    
}
