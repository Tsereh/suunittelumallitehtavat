/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author Kiril Tsereh
 */
public interface OutfitFactory {
    
    public abstract Housut makeHousut();
    public abstract Paita makePaita();
    public abstract Paahine makePaahine();
    public abstract Jalkineet makeJalkineet();
    
}
