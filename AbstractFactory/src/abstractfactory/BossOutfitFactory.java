/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author Kiril Tsereh
 */
public class BossOutfitFactory implements OutfitFactory {

    @Override
    public Housut makeHousut() {
        return new BossHousut();
    }

    @Override
    public Paita makePaita() {
        return new BossPaita();
    }

    @Override
    public Paahine makePaahine() {
        return new BossHattu();
    }

    @Override
    public Jalkineet makeJalkineet() {
        return new BossKengat();
    }
    
}
