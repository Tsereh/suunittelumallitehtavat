/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author Kiril Tsereh
 */
public class AbstractFactory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Outfit addut = new Outfit(new AdidasOutfitFactory());
        addut.luetteleVaatteet();
        Outfit boss = new Outfit(new BossOutfitFactory());
        boss.luetteleVaatteet();
    }
    
}
