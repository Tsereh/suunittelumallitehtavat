/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author Kiril Tsereh
 */
public class Outfit {
    
    private Paahine paahine;
    private Paita paita;
    private Housut housut;
    private Jalkineet jalkineet;
    
    public Outfit(OutfitFactory factory) {
        paahine = factory.makePaahine();
        paita = factory.makePaita();
        housut = factory.makeHousut();
        jalkineet = factory.makeJalkineet();
    }
    
    public void setOutfit(Paahine paahine, Paita paita, Housut housut, Jalkineet jalkineet) {
        this.paahine = paahine;
        this.paita = paita;
        this.housut = housut;
        this.jalkineet = jalkineet;
    }
    
    public void luetteleVaatteet() {
        System.out.println("Päälläni on " + paahine + ", " + paita + ", " + housut + " ja " + jalkineet + ".");
    }
    
}
