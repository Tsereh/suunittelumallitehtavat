/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.Scanner;

/**
 *
 * @author kiril
 */
public class Arvaaja extends Thread {
    
    private Memento memento;
    private String nimi;
    
    public Arvaaja(String nimi) {
        this.nimi = nimi;
    }
    
    public void setMemento(Memento memento) {
        this.memento = memento;
    }
    
    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Arvaaja " + nimi + " arvaa luku välillä 0-10");
        while(!Arvuuttaja.arvaa(memento, scanner.nextInt())) {
            System.out.println("Arvaaja " + nimi + " arvaa luku välillä 0-10");
        }
        System.out.println("Arvasit!");
    }
    
    
}
