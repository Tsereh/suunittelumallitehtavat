/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.Random;

/**
 *
 * @author kiril
 */
public class Arvuuttaja {
    public Memento joinGame() {
        Random random = new Random();
        int number = random.nextInt(10);
        System.out.println(number);
        return new Memento(number);
    }
    
    public static boolean arvaa(Memento memento, int arvaus) {
        return memento.getNumber() == arvaus;
    }
}
