/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author kiril
 */
public class Memento {
    
    private final int number;

    public Memento(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
