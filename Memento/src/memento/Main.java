/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author kiril
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Arvaaja pelaaja1 = new Arvaaja("1");
        Arvuuttaja arvuuttaja = new Arvuuttaja();
        pelaaja1.setMemento(arvuuttaja.joinGame());
        pelaaja1.start();
    }
    
}
