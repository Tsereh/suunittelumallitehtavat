/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author kiril
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Kello kello = new Kello();
        
        kello.getViisarit().setH(12);
        
        Kello kopiokello = (Kello) kello.clone();
        
        System.out.println(kello);
        System.out.println(kopiokello);
        
        kopiokello.getViisarit().setMin(30);
        
        System.out.println(kello);
        System.out.println(kopiokello);
        
       
        
        
    }
    
}
