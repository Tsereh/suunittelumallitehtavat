/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author kiril
 */
class Viisarit implements Cloneable {
    
    private int sec, min, h;

    public Viisarit(int sec, int min, int h) {
        this.sec = sec;
        this.min = min;
        this.h = h;
    }
    
    public Viisarit() {
        
    }
    

    public int getSec() {
        return sec;
    }

    public int getMin() {
        return min;
    }

    public int getH() {
        return h;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setH(int h) {
        this.h = h;
    }


    
    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            return super.clone();
        } catch(CloneNotSupportedException e) {
            return null;
        }
    }
    
}
