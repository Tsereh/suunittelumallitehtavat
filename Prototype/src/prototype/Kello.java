/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author kiril
 */
public class Kello implements Cloneable {
    
    private Viisarit viisarit;
    
    public Kello() {
        viisarit = new Viisarit();
    }
    
    public Viisarit getViisarit() {
        return viisarit;
    }

    public void setViisarit(Viisarit viisarit) {
        this.viisarit = viisarit;
    }
    
    @Override
    public String toString() {
        return viisarit.getH() + ":" + viisarit.getMin() + ":" + viisarit.getSec();
    }
    
    @Override
    public Object clone() {
        Kello kello;
        
        try {
            kello = (Kello)super.clone();
            kello.viisarit = (Viisarit)viisarit.clone();
            return kello;
        } catch(CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
