/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author kiril
 */
public class Proxy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Image> images = new ArrayList<>();
        images.add(new ProxyImage("HiRes_10MB_Photo1"));
        images.add(new ProxyImage("HiRes_10MB_Photo2"));
        images.add(new ProxyImage("HiRes_10MB_Photo3"));
        images.add(new ProxyImage("HiRes_10MB_Photo4"));
        images.add(new ProxyImage("HiRes_10MB_Photo5"));
        images.add(new ProxyImage("HiRes_10MB_Photo6"));
        images.add(new ProxyImage("HiRes_10MB_Photo7"));
        
        Scanner scanner = new Scanner(System.in);
        int pointer = 0;
        boolean running = true;
        System.out.println("Kuvakansio");
        images.get(pointer).displayImage();
        while(running) {
            System.out.println("[1] prev [2] next [3] reload [4] list all [5] exit");
            System.out.println("You are at: " + pointer);
            System.out.println("Action: ");
            
            String choice = scanner.nextLine();
            
            switch(choice) {
                case "1":
                    //Taakse
                    pointer--;
                    if(pointer < 0) {
                        pointer = 0;
                    }
                    images.get(pointer).displayImage();
                    break;
                case "2":
                    pointer++;
                    if(pointer > images.size() - 1) {
                        pointer = images.size();
                    }
                    images.get(pointer).displayImage();
                    break;
                case "3":
                    images.get(pointer).displayImage();
                    break;
                case "4":
                    for (Image image : images) {
                        image.showData();
                    }
                    break;
                case "5":
                    running = false;
                    break;
            }
            
        }
    }
    
}
