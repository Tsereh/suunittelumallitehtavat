/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Kiril Tsereh
 */
public class Decorator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws NoSuchAlgorithmException {
        Kasittelija perusKasittelija = new SimpleKasittelija();
        perusKasittelija.kirjoita("Ei salattua tekstia.", "eisalattu.txt");
        System.out.println(perusKasittelija.lue("eisalattu.txt"));
        
        Kasittelija salattuKasittelija = new SalaajaDecorator(new SimpleKasittelija());
        salattuKasittelija.kirjoita("Salattua tekstia.", "salattu.txt");
        System.out.println(perusKasittelija.lue("salattu.txt"));
        System.out.println(salattuKasittelija.lue("salattu.txt"));
    }
    
}
