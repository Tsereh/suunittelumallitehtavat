/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Kiril Tsereh
 */
public class SalaajaDecorator extends KasittelijaDecorator {
    KeyGenerator keygenerator = KeyGenerator.getInstance("Blowfish");
    private SecretKey secretkey = keygenerator.generateKey();
    
    public SalaajaDecorator(Kasittelija kasittelijaToBeDecorated) throws NoSuchAlgorithmException {
        super(kasittelijaToBeDecorated);
    }
    
    @Override
    public void kirjoita(String data, String path) {
        try {
            data = encrypt(data);
        } catch (Exception ex) {
            Logger.getLogger(SalaajaDecorator.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.kirjoita(data, path);
    }
    
    @Override
    public String lue(String path) {
        String data = "";
        try {
            data = decrypt(super.lue(path));
        } catch (Exception ex) {
            Logger.getLogger(SalaajaDecorator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }
    
    
    
    
    private String encrypt(String strClearText) throws Exception{
	String strData="";
	
	try {
 //           SecretKeySpec skeyspec=new SecretKeySpec("FEDCBA9876543210".getBytes(),"DES");
            Cipher cipher=Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, secretkey);
            byte[] encrypted=cipher.doFinal(strClearText.getBytes());
            byte[] encodedBytes = Base64.getEncoder().encode(encrypted);
            strData=new String(encodedBytes);
		
	} catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
	}
	return strData;
    }
    
    private String decrypt(String strEncrypted) throws Exception{
	String strData="";
	
	try {
//		SecretKeySpec skeyspec=new SecretKeySpec("FEDCBA9876543210".getBytes(),"DES");
		Cipher cipher=Cipher.getInstance("Blowfish");
		cipher.init(Cipher.DECRYPT_MODE, secretkey);
                byte[] decodedBytes = Base64.getDecoder().decode(strEncrypted.getBytes());
		byte[] decrypted=cipher.doFinal(decodedBytes);
		strData=new String(decrypted);
		
	} catch (Exception e) {
		e.printStackTrace();
		throw new Exception(e);
	}
	return strData;
    }
    
}
