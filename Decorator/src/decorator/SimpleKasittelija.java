/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author Kiril Tsereh
 */
public class SimpleKasittelija implements Kasittelija {
    private PrintWriter writer;

    @Override
    public void kirjoita(String data, String path) {
        try {
            FileWriter fw = new FileWriter(path);
            writer = new PrintWriter(fw, true);
        } catch (IOException e) {}
        writer.println(data);
    }

    @Override
    public String lue(String path) {
        String data = "";
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            data = new String(encoded, StandardCharsets.UTF_8);
        } catch (IOException e) {
            
        }
        return data;
    }
    
}
