/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Kiril Tsereh
 */
public interface Kasittelija {
    void kirjoita(String data, String path);
    String lue(String path);
}
