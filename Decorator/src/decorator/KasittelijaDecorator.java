/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Kiril Tsereh
 */
abstract class KasittelijaDecorator implements Kasittelija {
    protected Kasittelija kasittelijaToBeDecorated;
    
    public KasittelijaDecorator (Kasittelija kasittelijaToBeDecorated) {
        this.kasittelijaToBeDecorated = kasittelijaToBeDecorated;
    }
    
    @Override
    public void kirjoita(String data, String path) {
        kasittelijaToBeDecorated.kirjoita(data, path);
    }
    
    @Override
    public String lue(String path) {
        return kasittelijaToBeDecorated.lue(path);
    }
    
}
