/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Kiril Tsereh
 */
public class StateCharmander implements CharmanderIF {

    @Override
    public int flametrower() {
        return 5;
    }

    @Override
    public int firespin() {
        return 10;
    }

    @Override
    public int overheat() {
        return 15;
    }
    
    @Override
    public void evolve(StateContext sc) {
        sc.setState(new StateCharmeleon());
    }

    @Override
    public void accept(StateVisitor visitor) {
        visitor.visit(this);
    }
    
}
