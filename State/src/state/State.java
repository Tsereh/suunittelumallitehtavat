/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Kiril Tsereh
 */
public class State {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final StateContext sc = new StateContext();
        final StateVisitor visitor = new CharmanderVisitor();
        
        System.out.println("Hits " + sc.flametrower() + "hp");
        System.out.println("Hits " + sc.firespin() + "hp");
        System.out.println("Hits " + sc.overheat() + "hp");
        sc.accept(visitor);
        sc.evolve();
        
        System.out.println("Hits " + sc.flametrower() + "hp");
        System.out.println("Hits " + sc.firespin() + "hp");
        System.out.println("Hits " + sc.overheat() + "hp");
        sc.accept(visitor);
        sc.evolve();
        
        System.out.println("Hits " + sc.flametrower() + "hp");
        System.out.println("Hits " + sc.firespin() + "hp");
        System.out.println("Hits " + sc.overheat() + "hp");
        sc.accept(visitor);
        sc.evolve();
        
        System.out.println("Hits " + sc.flametrower() + "hp");
        System.out.println("Hits " + sc.firespin() + "hp");
        System.out.println("Hits " + sc.overheat() + "hp");
        sc.accept(visitor);
        sc.evolve();
    }
    
}
