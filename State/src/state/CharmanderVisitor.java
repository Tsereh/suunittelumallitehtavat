/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author kiril
 */
public class CharmanderVisitor implements StateVisitor {

    @Override
    public void visit(StateCharmander charmander) {
        System.out.println("1 bonus piste");
    }

    @Override
    public void visit(StateCharmeleon charmeleon) {
        System.out.println("2 bonus piste");
    }

    @Override
    public void visit(StateCharizard charizard) {
        System.out.println("3 bonus piste");
    }
    
}
