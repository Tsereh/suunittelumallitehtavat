/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Kiril Tsereh
 */
public interface CharmanderIF {
    int flametrower();
    int firespin();
    int overheat();
    void evolve(StateContext sc);
    void accept(StateVisitor visitor);
}
