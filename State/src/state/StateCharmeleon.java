/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Kiril Tsereh
 */
public class StateCharmeleon implements CharmanderIF {

    @Override
    public int flametrower() {
        return 10;
    }

    @Override
    public int firespin() {
        return 20;
    }

    @Override
    public int overheat() {
        return 30;
    }
    
    @Override
    public void evolve(StateContext sc) {
        sc.setState(new StateCharizard());
    }

    @Override
    public void accept(StateVisitor visitor) {
        visitor.visit(this);
    }
    
}
