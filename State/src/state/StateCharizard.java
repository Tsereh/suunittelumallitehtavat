/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Kiril Tsereh
 */
public class StateCharizard implements CharmanderIF {

    @Override
    public int flametrower() {
        return 20;
    }

    @Override
    public int firespin() {
        return 40;
    }

    @Override
    public int overheat() {
        return 60;
    }
    
    @Override
    public void evolve(StateContext sc) {
        // Maxevolve
    }

    @Override
    public void accept(StateVisitor visitor) {
        visitor.visit(this);
    }
    
}
