/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author kiril
 */
public interface StateVisitor {
    public void visit(StateCharmander charmander);
    public void visit(StateCharmeleon charmeleon);
    public void visit(StateCharizard charizard);
}
