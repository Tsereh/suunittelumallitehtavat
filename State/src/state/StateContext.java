/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Kiril Tsereh
 */
public class StateContext implements CharmanderIF {
    private CharmanderIF state;
    StateContext() {
        setState(new StateCharmander());
    }
    
    void setState(final CharmanderIF newState) {
        state = newState;
    }
    
    public int flametrower() {
        return state.flametrower();
    }

    public int firespin() {
        return state.firespin();
    }

    public int overheat() {
        return state.overheat();
    }
    
    public void evolve() {
        state.evolve(this);
    }

    @Override
    public void accept(StateVisitor visitor) {
        state.accept(visitor);
    }

    @Override
    public void evolve(StateContext sc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
