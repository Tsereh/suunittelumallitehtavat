/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kiril Tsereh
 */
public class Strategy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Converter converter = new Converter(new ListConverterStrategy1());
        List list = new ArrayList();
        for(int i=0; i<20; i++) {
            list.add("a");
        }
        System.out.println(converter.listToString(list));
        
        converter.setStrategy(new ListConverterStrategy2());
        System.out.println(converter.listToString(list));
        
        converter.setStrategy(new ListConverterStrategy3());
        System.out.println(converter.listToString(list));
    }
    
}
