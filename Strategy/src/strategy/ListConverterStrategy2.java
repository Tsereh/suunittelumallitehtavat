/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.List;

/**
 *
 * @author kiril
 */
public class ListConverterStrategy2 implements ListConverterStrategy {

    @Override
    public String listToString(List list) {
        String string = "";
        
        String[] array = new String[list.size()];
        list.toArray(array);
        
        int i = 0;
        for(String s : array) {
            string = string + s;
            if((i+1)%2==0) {
                string = string + "\n";
            }
            i++;
        }
        
        return string;
    }
    
}
