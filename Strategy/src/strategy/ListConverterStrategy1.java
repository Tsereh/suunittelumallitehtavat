/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author kiril
 */
public class ListConverterStrategy1 implements ListConverterStrategy {

    @Override
    public String listToString(List list) {
        //läpikäynti iteraattorilla
        String string = "";
        
        ListIterator<String> iterator = list.listIterator();
        
        while(iterator.hasNext()) {
            string = string + iterator.next() + "\n";
        }
        
        return string;
    }
    
}
