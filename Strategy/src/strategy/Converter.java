/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.List;

/**
 *
 * @author kiril
 */
public class Converter {
    private ListConverterStrategy strategy;
    
    public Converter(ListConverterStrategy strategy) {
        this.strategy = strategy;
    }
    
    public String listToString(List<String> list) {
        return strategy.listToString(list);
    }

    public void setStrategy(ListConverterStrategy strategy) {
        this.strategy = strategy;
    }
    
}
