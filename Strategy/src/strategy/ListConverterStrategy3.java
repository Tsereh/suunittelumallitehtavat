/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.List;

/**
 *
 * @author kiril
 */
public class ListConverterStrategy3 implements ListConverterStrategy {

    @Override
    public String listToString(List<String> list) {
        String string = "";
        
        for(int i=0; i<list.size(); i++) {
            string = string + list.get(i);
            if((i+1)%3==0) {
                string = string + "\n";
            }
        }
        
        return string;
    }
    
}
