/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author kiril
 */
public class IteratorSaje implements Runnable {
    List list;
    Iterator iterator;

    @Override
    public void run() {
        while(this.iterator.hasNext()) {
            Object obj = iterator.next();
            System.out.println(Thread.currentThread().getName() + ", " + obj + ", " + iterator);
            try {
                Thread.sleep(100);
            } catch(InterruptedException e) {
                System.out.println(e);
            }
            
            System.out.println("Lopetetaan " + Thread.currentThread().getName());
            if (list.indexOf(obj) != -1) {
                list.set(list.indexOf(obj), "A");
            }
            list.set(4, "asd");
        }
    }
    
    public void setList(List list) {
        this.list = list;
        this.iterator = this.list.iterator();
    }
    
    public void setIterator(Iterator iterator) {
        this.iterator = iterator;
    }
    
    public Iterator getIterator() {
        return iterator;
    }
    
}
