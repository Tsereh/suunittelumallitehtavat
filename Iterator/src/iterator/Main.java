/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kiril
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        
        
        // Kaksi säjettä molemmilla omat iteraattorit
        IteratorSaje iterator1 = new IteratorSaje();
        IteratorSaje iterator2 = new IteratorSaje();
        
        iterator1.setList(list);
        iterator2.setList(list);
        
        new Thread(iterator1).start();
        new Thread(iterator2).start();
        
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }

        
        // Kaksi jäjettä yhteisellä iteraattorilla
        iterator1.setList(list);
        iterator2.setList(list);
        iterator2.setIterator(iterator1.getIterator());
        
        new Thread(iterator1).start();
        new Thread(iterator2).start();
        list.set(5, "A");
        list.set(6, "B");
        list.set(7, "C");
        list.set(8, "D");
        
        
        
        
    }
    
}
