/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author Kiril Tsereh
 */
public class Singleton {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logger logger1 = Logger.getInstance();
        Logger logger2 = Logger.getInstance();
        Logger logger3 = Logger.getInstance();
        logger1.logDeposit("0001", 80.5);
        logger2.logWithdraw("0002", 100);
        logger1.logTransfer("0001", "0003", 40);
        logger3.logDeposit("0004", 56.74);
        logger2.logWithdraw("0005", 30);
    }
    
}
