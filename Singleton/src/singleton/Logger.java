/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Kiril Tsereh
 */
public class Logger {
	
	private static Logger logger = null;
	
	private final String logFile = "test_log.txt";
	private PrintWriter writer;
	
	private Logger() {
		try {
			FileWriter fw = new FileWriter(logFile);
		    writer = new PrintWriter(fw, true);
		} catch (IOException e) {}
	}
	
	public static synchronized Logger getInstance(){
		if(logger == null)
			logger = new Logger();
		return logger;
	}
	
	public void logWithdraw (String account, double amount) {
		writer.println("NOSTO (" + account + "): " + amount + "€");
	}
	
	public void logDeposit (String account, double amount) {
		writer.println("PANO (" + account + "): " + amount + "€");
	}
	
	public void logTransfer (String fromAccount, String toAccount, double amount) {
		writer.println("SIIRTO (" + fromAccount + "->" + toAccount + "): " + amount + "€");
	}

}
