/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author kiril
 */
public class VihollisRobottiAdapteri implements Vihollinen {
    
    VihollisRobotti robotti;
    
    public VihollisRobottiAdapteri(VihollisRobotti robotti) {
        this.robotti = robotti;
    }

    @Override
    public void ammu() {
        robotti.lyo();
    }

    @Override
    public void ajaEteen() {
        robotti.kaveleEteen();
    }

    @Override
    public void liitaKuljettaja(String kuljettajaNimi) {
        System.out.println("Robottiin ei voi liittää kuljettajaa.");
    }
    
}
