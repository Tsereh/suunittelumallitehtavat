/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author kiril
 */
public class VihollisRobotti {
    
    public void lyo() {
        System.out.println("Vihollisrobotti aiheutti " + 2 + "hp vaurion lyömällä.");
    }
    
    public void kaveleEteen() {
        System.out.println("Vihollisrobotti käveli " + 2 + "metriä eteenpäin.");
    }
    
    // robotti ei tarvitse kuljettajaa
    
}
