/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author kiril
 */
public class VihollisTankki implements Vihollinen {

    @Override
    public void ammu() {
        System.out.println("Vihollistankki aiheutti " + 5 + "hp vaurion ampumalla.");
    }

    @Override
    public void ajaEteen() {
        System.out.println("Vihollistankki ajoi " + 5 + "metriä eteenpäin.");
    }

    @Override
    public void liitaKuljettaja(String kuljettajaNimi) {
        System.out.println("Vihollistankkiin liitettiin " + kuljettajaNimi + ".");
    }
    
}
