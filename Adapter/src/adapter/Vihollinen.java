/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author kiril
 */
public interface Vihollinen {
    
    public void ammu();
    public void ajaEteen();
    public void liitaKuljettaja(String kuljettajaNimi);
    
}
