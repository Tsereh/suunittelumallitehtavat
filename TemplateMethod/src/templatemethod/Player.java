/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package templatemethod;

/**
 *
 * @author Kiril Tsereh
 */
public class Player {
    private int knockOutNumber;
    private boolean knockedOut = false;

    public void setKnockOutNumber(int knockOutNumber) {
        this.knockOutNumber = knockOutNumber;
    }

    public void setKnockedOut(boolean knockedOut) {
        this.knockedOut = knockedOut;
    }

    public int getKnockOutNumber() {
        return knockOutNumber;
    }

    public boolean isKnockedOut() {
        return knockedOut;
    }
    
    
}
