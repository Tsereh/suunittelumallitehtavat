/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package templatemethod;

import java.util.concurrent.ThreadLocalRandom;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Kiril Tsereh
 */
public class DiceKnockOut extends Game {
    private Scanner scanner = new Scanner(System.in);
    private ArrayList<Player> players = new ArrayList<Player>();

    @Override
    void initializeGame() {
        System.out.println("Directions for play:");
        System.out.println("1. Each player chooses a “knock out number” – either 6, 7, 8, or 9. More than one player can choose the same number.");
        System.out.println("2. Players take turns throwing both dice, once each turn. Add the number of both dice for the score.");
        System.out.println("3. If a player throws a 6, 7, 8 or 9, they are knocked out of the game until the next round.");
        for(int i=0; i<playersCount; i++) {
            int kon=0;
            while(kon>9 || kon<6) {
                System.out.print("Player " + i + ", please type in your knock out number between 6 and 9: ");
                kon = scanner.nextInt();
                scanner.nextLine();
            }
            Player p = new Player();
            p.setKnockOutNumber(kon);
            players.add(p);
        }
    }

    @Override
    void makePlay(int player) {
        if (!players.get(player).isKnockedOut()) {
            System.out.println();
            System.out.println("Player " + player + " turn.");
            int randomNum1 = ThreadLocalRandom.current().nextInt(1, 6 + 1);
            System.out.println("Dice nr1 gives: " + randomNum1);
            int randomNum2 = ThreadLocalRandom.current().nextInt(1, 6 + 1);
            System.out.println("Dice nr2 gives: " + randomNum2);
            int sum = randomNum1+randomNum2;
            System.out.println("Dices sum is " + sum);
            if(players.get(player).getKnockOutNumber()==sum) {
                System.out.println("Player " + player + " gets knocked out!");
                players.get(player).setKnockedOut(true);
            }
        }
    }

    @Override
    boolean endOfGame() {
        int notKnockedOutPlayers = 0;
        for(int i=0; i<playersCount; i++) {
            if(!players.get(i).isKnockedOut()) {
                notKnockedOutPlayers++;
            }
        }
        if(notKnockedOutPlayers==1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    void printWinner() {
        for(int i=0; i<playersCount; i++) {
            if(!players.get(i).isKnockedOut()) {
                System.out.println("Player " + i + " wins!");
            }
        }
    }
    
}
