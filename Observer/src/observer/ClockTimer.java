/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.Observable;

/**
 *
 * @author Kiril Tsereh
 */
public class ClockTimer extends Observable {
    private int hour, minute, second = 0;

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }
    
    public void tick() {
        if(second < 59) {
            second++;
        } else {
            second = 0;
            if(minute<59) {
                minute++;
            } else {
                minute=0;
                hour++;
            }
        }
        setChanged();
        notifyObservers(this);
    }
    
}
