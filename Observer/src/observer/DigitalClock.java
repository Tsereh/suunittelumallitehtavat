
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Kiril Tsereh
 */
public class DigitalClock implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof ClockTimer) {
            System.out.println(((ClockTimer) o).getHour() + ":" + ((ClockTimer) o).getMinute() + ":" + ((ClockTimer) o).getSecond());
        }
    }
    
}
