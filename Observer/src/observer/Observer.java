/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

/**
 *
 * @author Kiril Tsereh
 */
public class Observer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ClockTimer ct = new ClockTimer();
        DigitalClock dc = new DigitalClock();
        ct.addObserver(dc);
        
        for(int i = 0; i<36000; i++) {
            ct.tick();
        }
    }
    
}
