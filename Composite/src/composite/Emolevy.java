/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kiril Tsereh
 */
public class Emolevy implements Laiteosa {
    private double hinta;
    List<Laiteosa> komponentit = new ArrayList<Laiteosa>();
    
    public Emolevy(double hinta) {
        this.hinta = hinta;
    }

    @Override
    public double getHinta() {
        double kh = this.hinta;
            for(Laiteosa k : komponentit) {
                kh = kh+k.getHinta();
            }
        return kh;
    }

    @Override
    public void addLaiteosa(Laiteosa lo) {
        komponentit.add(lo);
    }
    
}
