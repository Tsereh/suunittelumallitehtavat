/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

/**
 *
 * @author Kiril Tsereh
 */
public class Composite {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pc pc = new Pc();
        
        Laiteosa kotelo = new Kotelo(39.90);
        Laiteosa polttavalevyasema = new Levyasema(34.90);
        kotelo.addLaiteosa(polttavalevyasema);
        Laiteosa tuuletin = new Jäähdytys(29.00);
        kotelo.addLaiteosa(tuuletin);
        
        pc.addOsa(kotelo);
        
        Laiteosa emolevy = new Emolevy(299.00);
        Laiteosa prosessori = new Prosessori(384.90);
        emolevy.addLaiteosa(prosessori);
        Laiteosa muistipiiri = new Muistipiiri(189.00);
        emolevy.addLaiteosa(muistipiiri);
        Laiteosa verkkokortti = new Verkkokortti(29.00);
        emolevy.addLaiteosa(verkkokortti);
        
        pc.addOsa(emolevy);
        
        Laiteosa näytönohjain = new Näytönohjain(409.00);
        
        pc.addOsa(näytönohjain);
        
        System.out.println("Kokonaishinta: " + pc.getHinta());
        
    }
    
}
