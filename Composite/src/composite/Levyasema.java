/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

/**
 *
 * @author Kiril Tsereh
 */
public class Levyasema implements Laiteosa {
    private double hinta;
    
    public Levyasema(double hinta) {
        this.hinta = hinta;
    }

    @Override
    public double getHinta() {
        return hinta;
    }

    @Override
    public void addLaiteosa(Laiteosa lo) {
        throw new RuntimeException("Ei voida lisätä laiteosaa yksinkertaiseen laiteosaan");
    }
}
