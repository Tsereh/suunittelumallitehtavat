/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kiril Tsereh
 */
public class Pc {
    private List<Laiteosa> osat = new ArrayList<Laiteosa>();
    
    public Pc() {
        
    }
    
    public double getHinta() {
        double h = 0;
        for(Laiteosa o : osat) {
            System.out.println(o.getHinta());
            h = h+o.getHinta();
        }
        return h;
    }
    
    public void addOsa(Laiteosa lo) {
        osat.add(lo);
    }
    
}
